package mz.com.project.data.model;

import com.google.gson.annotations.SerializedName;

public class Info {
    @SerializedName("title")
    String title;
    @SerializedName("name")
    String name;
    @SerializedName("description")
    String description;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
