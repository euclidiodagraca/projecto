package mz.com.project.data;

import java.util.ArrayList;

import mz.com.project.data.model.Info;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface APIConf{
    @GET("info")
    Call<ArrayList<Info>> getInfos();

    @POST("info")
    Call<Info> createInfo(@Body Info info);
}
